﻿using System;

namespace Lab2
{
    class Program
    {
        static void Main()
        {
            Console.Title = "База данных \"Студенты\"";
            DataBase students = new DataBase();
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\DataBase.txt";
            students.Read(new System.IO.FileInfo(path));
            while (true)
            {
                Console.Clear();
                students.Show();
                Console.WriteLine("1. Добавить запись\n2. Изменить запись\n3. Удалить запись\n4. Сортировка\n5. Поиск\n6. Статистика баллов студентов\n7. Записать изменения на диск\n0. Выход");
                try
                {
                    int menu = int.Parse(Console.ReadLine());
                    if (menu == 0)
                        break;
                    switch (menu)
                    {
                        case 1:
                            {
                                students.Add();
                                break;
                            }
                        case 2:
                            {
                                students.Edit();
                                break;
                            }
                        case 3:
                            {
                                students.Delete();
                                break;
                            }
                        case 4:
                            {
                                while (true)
                                {
                                    Console.Clear();
                                    Console.WriteLine("1. Сортировка по ФИО\n2. Сортировка по дате рождения");
                                    try
                                    {
                                        int menuSort = int.Parse(Console.ReadLine());
                                        switch (menuSort)
                                        {
                                            case 1:
                                                {
                                                    while (true)
                                                    {
                                                        Console.Clear();
                                                        Console.WriteLine("1. Сортировка по возрастанию\n2. Сортировка по убыванию");
                                                        try
                                                        {
                                                            int menuReverse = int.Parse(Console.ReadLine());
                                                            switch (menuReverse)
                                                            {
                                                                case 1:
                                                                    {
                                                                        students.SortName(false);
                                                                        break;
                                                                    }
                                                                case 2:
                                                                    {
                                                                        students.SortName(true);
                                                                        break;
                                                                    }
                                                            }
                                                            break;
                                                        }
                                                        catch (Exception)
                                                        {
                                                            continue;
                                                        }
                                                    }
                                                    break;
                                                }
                                            case 2:
                                                {
                                                    while (true)
                                                    {
                                                        Console.Clear();
                                                        Console.WriteLine("1. Сортировка по возрастанию\n2. Сортировка по убыванию");
                                                        try
                                                        {
                                                            int menuReverse = int.Parse(Console.ReadLine());
                                                            switch (menuReverse)
                                                            {
                                                                case 1:
                                                                    {
                                                                        students.SortBirthday(false);
                                                                        break;
                                                                    }
                                                                case 2:
                                                                    {
                                                                        students.SortBirthday(true);
                                                                        break;
                                                                    }
                                                            }
                                                            break;
                                                        }
                                                        catch (Exception)
                                                        {
                                                            continue;
                                                        }
                                                    }
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                    catch (Exception)
                                    {
                                        continue;
                                    }
                                }
                                break;
                            }
                        case 5:
                            {
                                while (true)
                                {
                                    Console.Clear();
                                    Console.WriteLine("1. Поиск по ФИО\n2. Поиск по дате рождения");
                                    try
                                    {
                                        int menuFind = int.Parse(Console.ReadLine());
                                        switch (menuFind)
                                        {
                                            case 1:
                                                {
                                                    students.FindName();
                                                    break;
                                                }
                                            case 2:
                                                {
                                                    students.FindBirthday();
                                                    break;
                                                }
                                        }
                                        break;
                                    }
                                    catch (Exception)
                                    {
                                        continue;
                                    }
                                }
                                break;
                            }
                        case 6:
                            {
                                students.MaxMinAver();
                                break;
                            }
                        case 7:
                            {
                                students.Write(new System.IO.FileInfo(path));
                                break;
                            }
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }
    }
}
