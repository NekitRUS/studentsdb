﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Lab2
{
    class DataBase
    {
        struct Student
        {
            public string name;
            public DateTime birthday;
            public string institute;
            public string group;
            public byte course;
            public float mark;
            public override string ToString()
            {
                return name + " " + birthday.ToString("dd.MM.yyyy") + " " + institute + " " + group + " " + course.ToString() + " " + mark.ToString();
            }
            public static int SortByName(Student obj1, Student obj2)
            {
                return obj1.name.CompareTo(obj2.name);
            }
            public static int SortByBirthday(Student obj1, Student obj2)
            {
                return obj1.birthday.Date.CompareTo(obj2.birthday.Date);
            }
        }

        List<Student> list;

        public void Read(FileInfo path)
        {
            list = new List<Student>();
            if ((path.Extension != ".txt") || (!path.Exists))
            {
                Console.Clear();
                Console.WriteLine("Программа поддерживает работу только с .txt-файлами!");
                Console.ReadKey();
            }
            else
            {
                try
                {
                    StreamReader sr = new StreamReader(path.FullName, System.Text.Encoding.GetEncoding(1251));
                    while (!sr.EndOfStream)
                    {
                        Student temp;
                        string[] elements = sr.ReadLine().Split(' ');
                        temp.name = elements[0] + " " + elements[1];
                        temp.birthday = DateTime.Parse(elements[2]);
                        temp.institute = elements[3];
                        temp.group = elements[4];
                        temp.course = byte.Parse(elements[5]);
                        temp.mark = float.Parse(elements[6]);
                        list.Add(temp);
                    }
                    sr.Close();
                }
                catch (Exception)
                {
                    Console.Clear();
                    Console.WriteLine("Ошибка при чтении из файла!");
                    Console.ReadKey();
                }
            }
        }

        public void Write(FileInfo path)
        {
            if (list != null)
            {
                try
                {
                    StreamWriter sw = new StreamWriter(path.FullName, false, System.Text.Encoding.GetEncoding(1251));
                    foreach (Student temp in list)
                        sw.WriteLine(temp);
                    sw.Close();
                    Console.WriteLine("Данные были успешно записаны на диск!");
                    Console.ReadKey();
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка при записи файла!");
                    Console.ReadKey();
                }
            }
        }

        public void Show()
        {
            int number = 1;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("№      ФИО                 Дата рождения    Институт   Группа   Курс   Балл ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            foreach (Student temp in list)
                Console.WriteLine("{0,-2:d} {1,-24} {2,-16:dd MMM yyyy} {3,-9} {4,-9} {5,-4:d} {6:f3}", number++, temp.name, temp.birthday, temp.institute, temp.group, temp.course, temp.mark);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("____________________________________________________________________________");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void MaxMinAver()
        {
            Show();
            Console.WriteLine();
            if (list.Count != 0)
            {
                float max, min, average, sum = 0;
                max = list.Max<Student>(temp => temp.mark);
                min = list.Min<Student>(temp => temp.mark);
                foreach (Student temp in list)
                    sum += temp.mark;
                average = sum / list.Count;
                Console.WriteLine("Максимальный балл равен {0:f3}\nМинимальный балл равен {1:f3}\nСумма балло студентов равна {2:f3}\nСредний балл равен {3:f3}", max, min, sum, average);
            }
            else
                Console.WriteLine("База данных пуста!");
            Console.ReadKey();
        }

        public void SortName(bool reverse)
        {
            if (list.Count != 0)
            {
                list.Sort(Student.SortByName);
                if (reverse)
                    list.Reverse();
            }
            else
            {
                Console.WriteLine("База данных пуста!");
                Console.ReadKey();
            }
        }

        public void SortBirthday(bool reverse)
        {
            if (list.Count != 0)
            {
                list.Sort(Student.SortByBirthday);
                if (reverse)
                    list.Reverse();
            }
            else
            {
                Console.WriteLine("База данных пуста!");
                Console.ReadKey();
            }
        }

        public void FindName()
        {
            string pattern;
            Console.Clear();
            Console.WriteLine("Введите фамилию студента...");
            pattern = Console.ReadLine();
            Console.WriteLine("Введите имя студента...");
            pattern += " " + Console.ReadLine();
            Student result = list.Find(temp => temp.name == pattern);
            if (result.name != null)
            {
                int index = list.FindIndex(temp => temp.Equals(result));
                Console.Clear();
                Console.WriteLine("Найдено совпадение:");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("№      ФИО                 Дата рождения    Институт   Группа   Курс   Балл ");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("{0,-2:d} {1,-24} {2,-16:dd MMM yyyy} {3,-9} {4,-9} {5,-4:d} {6:f3}", ++index, result.name, result.birthday, result.institute, result.group, result.course, result.mark);
                Console.ForegroundColor = ConsoleColor.White;
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Совпадений не найдено!");
                Console.ReadKey();
            }
        }

        public void FindBirthday()
        {
            Console.Clear();
            Console.WriteLine("Введите дату рождения студента в формате \"дд мм гггг\"...");
            try
            {
                DateTime pattern = DateTime.Parse(Console.ReadLine());
                Student result = list.Find(temp => temp.birthday == pattern);
                if (result.name != null)
                {
                    int index = list.FindIndex(temp => temp.Equals(result));
                    Console.Clear();
                    Console.WriteLine("Найдено совпадение:");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("№      ФИО                 Дата рождения    Институт   Группа   Курс   Балл ");
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("{0,-2:d} {1,-24} {2,-16:dd MMM yyyy} {3,-9} {4,-9} {5,-4:d} {6:f3}", ++index, result.name, result.birthday, result.institute, result.group, result.course, result.mark);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.ReadKey();
                }
                else
                {
                    Console.WriteLine("Совпадений не найдено!");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка!\n" + ex.Message);
                Console.ReadKey();
            }
        }

        public void Delete()
        {
            Console.Clear();
            Show();
            Console.WriteLine("Введите номер удаляемой записи");
            int index;
            try
            {
                index = int.Parse(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("Ошибка ввода!");
                Console.ReadKey();
                return;
            }
            if ((index < 1) || (index > list.Count))
            {
                Console.WriteLine("Элемента с таким номером не существует!");
                Console.ReadKey();
            }
            else
                list.RemoveAt(--index);
        }

        public void Add()
        {
            Student temp;
            while (true)
            {
                string name;
                Console.Clear();
                try
                {
                    Console.WriteLine("Введите фамилию студента...");
                    name = Console.ReadLine();
                    if (name == "")
                        throw new Exception();
                    Console.WriteLine("Введите имя студента...");
                    temp.name = name + " " + Console.ReadLine();
                    Console.WriteLine("Введите дату рождения студента в формате \"дд мм гггг\"...");
                    temp.birthday = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Введите институт...");
                    temp.institute = Console.ReadLine();
                    if (temp.institute == "")
                        throw new Exception();
                    Console.WriteLine("Введите группу...");
                    temp.group = Console.ReadLine();
                    if (temp.group == "")
                        throw new Exception();
                    Console.WriteLine("Введите курс...");
                    temp.course = Byte.Parse(Console.ReadLine());
                    Console.WriteLine("Введите баллы студента...");
                    temp.mark = float.Parse(Console.ReadLine());
                    list.Add(temp);
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка ввода!");
                    Console.ReadKey();
                    continue;
                }
            }
        }

        public void Edit()
        {
            Console.Clear();
            Show();
            Console.WriteLine("Введите номер изменяемой записи");
            int index;
            try
            {
                index = int.Parse(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("Ошибка ввода!");
                Console.ReadKey();
                return;
            }
            if ((index < 1) || (index > list.Count))
            {
                Console.WriteLine("Элемента с таким индексом не существует!");
                Console.ReadKey();
                return;
            }
            Add();
            list[--index] = list[list.Count - 1];
            list.RemoveAt(list.Count - 1);
        }

        /*public void Show()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WindowWidth = 90;
            int number = 1;
            Console.WriteLine(" №      ФИО                   Дата рождения   Институт   Группа   Курс   Балл ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            for (int i = 0; i < list.Count; i++)
            {
                Console.Write("╔══╦════════════════════════╦════════════════╦═════════╦═════════╦════╦══════════╗\n");
                Console.Write("║{0,-2:d}║{1,-24}║{2,-16:dd MMM yyyy}║{3,-9}║{4,-9}║{5,-4:d}║{6:f3}     ║\n", number++, list[i].name, list[i].birthday, list[i].institute, list[i].group, list[i].course, list[i].mark);
                Console.Write("╚══╩════════════════════════╩════════════════╩═════════╩═════════╩════╩══════════╝\n");
            }
            Console.ForegroundColor = ConsoleColor.White;
        }*/
    }
}
