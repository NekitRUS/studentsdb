Приложение для работы с БД "Студенты".
 
Структура БД:
 
* № (номер записи - элемента);
 
* ФИО студента;
 
* дата рождения;
 
* институт;
 
* группа;
 
* курс;
 
* средний балл.
 
Работа с БД:
 
* добавление, изменение, удаление записи;
 
* прямая и обратная сортировка по полям "ФИО" и "дата рождения";
 
* поиск элемента по полям "ФИО" и "дата рождения";
 
* нахождение max, min, average и sum по полю "средний балл".
 
Данные БД хранятся в файле.
 
![Безымянный.png](https://bitbucket.org/repo/yLErbd/images/2825154948-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)
 
![Безымянный2.png](https://bitbucket.org/repo/yLErbd/images/1154164063-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B92.png)
 
![Безымянный3.png](https://bitbucket.org/repo/yLErbd/images/476314082-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B93.png)
 
![Безымянный4.png](https://bitbucket.org/repo/yLErbd/images/3518793059-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B94.png)
 
![Безымянный5.png](https://bitbucket.org/repo/yLErbd/images/2525984224-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B95.png)